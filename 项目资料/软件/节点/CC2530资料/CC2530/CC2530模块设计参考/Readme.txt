XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
X   	Artwork and documentation done by: 	tik				X
X									X
X	TEXAS INSTRUMENTS NORWAY LPW              				X
X  									X
X	Address: Gaustadalléen 21    0349 OSLO                                       		X
X	Phone  : (+47) 22 95 85 44   Fax :  (+47) 22 95 89 05                   		X
X   	web: www.ti.com/lpw                            	 			X
X                                                                           	 			X
XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

PCB NAME : CC2530EM_DISCRETE	
REVISION: 1.3.1
DATE: 2009-04-23

PCB DESCRIPTION: 2 LAYER PCB 0.8 MM Nominal thickness with 35um Cu in each layer
		Dielectric constant for FR4 is 4.5
		Dimensions in manufacturing files are in mils (0.001 inch)
              	DOUBLE SIDE SOLDER MASK,
               	DOUBLE SIDE SILKSCREEN,
               	8 MIL MIN TRACE WIDTH AND 8 MIL MIN ISOLATION.

Change list
1.0 initial revision
1.3.1 improved power routing on pin 10, 27, 28 and 29. Added dual via as a simple RF block on dignital IO lines. 
	
                 
FILE NAME            				DESCRIPTION                               			FILE TYPE

PCB MANUFACTURING FILES:
L1.SPL               				LAYER 1 COMPONENT SIDE/POSITIVE            		EXT. GERBER
L2.SPL	              			LAYER 2 SOLDER SIDE/POSITIVE               		EXT. GERBER
STOPCOMP.SPL         			SOLDER MASK COMPONENT SIDE/NEGATIVE             	EXT. GERBER
STOPSOLD.SPL         			SOLDER MASK SOLDER SIDE/NEGATIVE                	EXT. GERBER
SILKCOMP.SPL         			SILKSCREEN COMPONENT SIDE/POSITIVE            	EXT. GERBER
SILKSOLD.SPL				SILKSCREEN SOLDER SIDE/POSITIVE			EXT. GERBER
GERBER.REP				DRILL and NC DRILL REPORT				ASCII
DRILL.SPL	            			DRILL/MECHANICAL DRAWING                  		EXT. GERBER
NCDRILL.SPL				NC DRILL THROUGH HOLE                     		EXCELLON			
EXT_GERBER.USR	     			PCB PHOTOPLOTTER DEFINITION FILE			ASCII
CNC.USR		     			NC DRILL DEVICE FILE				ASCII

PCB ASSEMBLY FILES:
CC2530EM_1_3_DISCRETE_PARTLIST.XLS	              PART LIST					ASCII
P&P_COMP.REP				PICK AND PLACE COORDINATES, COMPONENT SIDE	ASCII
P&P_SOLD.REP				PICK AND PLACE COORDINATES, SOLDER SIDE		ASCII
PASTCOMP.SPL         			SOLDER PASTE COMPONENT SIDE               		EXT. GERBER
PASTSOLD.SPL				SOLDER PASTE SOLDER SIDE			EXT. GERBER
ASSYCOMP.SPL        			ASSEMBLY DRAWING COMPONENT SIDE           		EXT. GERBER
ASSYSOLD.SPL				ASSEMBLY DRAWING SOLDER SIDE			EXT. GERBER

PDF FILES:
CC2530EM_1_3_DISCRETE_SCHEMATIC.PDF	Circuit Diagram
CC2530EM_1_3_DISCRETE_LAYOUT.PDF	              Layout Diagram

CADSTAR FILES:
CC2530EM_1_3_DISCRETE.SCM	              	Cadstar Schematic file
CC2530EM_1_3_DISCRETE.CSA		              Cadstar Shematic archive
CC2530EM_1_3_DISCRETE.PCB		              Cadstar layout file
CC2530EM_1_3_DISCRETE.CPA		              Cadstar PCB archive

README.TXT           			THIS FILE                                 			ASCII

END.
